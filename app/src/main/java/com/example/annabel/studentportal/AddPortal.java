package com.example.annabel.studentportal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class AddPortal extends AppCompatActivity {

    private EditText titleInput;
    private EditText urlInput;
    private Button addButton;
    private String title;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_portal);

        titleInput = findViewById(R.id.titleInput);
        urlInput = findViewById(R.id.urlInput);
        addButton = findViewById(R.id.addButton);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title = titleInput.getText().toString();
                url = urlInput.getText().toString();

                if (!title.isEmpty() || !url.isEmpty()){
                    Intent data = new Intent();
                    data.putExtra("url", url);
                    data.putExtra("title", title);
                    setResult(Activity.RESULT_OK, data);
                    finish();
                }
            }
        });
    }

}
