package com.example.annabel.studentportal;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements PortalAdapter.PortalClickListener{

    public List<Portal> portals;
    private RecyclerView recyclerView;
    private PortalAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        portals = new ArrayList<>();
        portals.add(new Portal("HvA", "http://www.hva.nl/"));
        portals.add(new Portal("Rooster", "https://rooster.hva.nl/"));
        portals.add(new Portal("VLO", "https://vlo.informatica.hva.nl/"));
        updateUI();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddPortal.class);
                startActivityForResult(intent, 1111);
            }
        });
    }
    private void updateUI(){
        if(adapter == null){
            adapter = new PortalAdapter(portals, this);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String portalTitle = data.getExtras().getString("title");
            String portalUrl = data.getExtras().getString("url");

            portals.add(new Portal(portalTitle, portalUrl));
            this.updateUI();
        }
    }


    public void portalOnClick(int i) {
        Intent intent = new Intent(MainActivity.this, PortalWebview.class);
        intent.putExtra("url", portals.get(i).getUrl());
        startActivity(intent);
    }
}
